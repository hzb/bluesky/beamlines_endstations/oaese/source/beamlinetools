from bessyii.magics.standard_magics import BlueskyMagicsBessy
from bessyii.magics.standard_magics_utils import get_imported_objects, create_magics_from_list, PlotSelect
from IPython import get_ipython
from .base import RE
from .beamline import *

from .devices_configuration import devices_dict

print('\n\nLOADING magics.py')

# Create aliases for standard plans
from .plans import *
count_plan = count
grid_scan_plan = grid_scan
scan_plan = scan

# Retrieve plan names from plans.py
plan_names = ['grid_scan', 'scan','count']

# Retrieve plan names
create_magics_from_list(plan_names)

get_ipython().register_magics(BlueskyMagicsBessy(RE, 
                                get_ipython(), 
                                database_name ="client", 
                                label_axes_dict=devices_dict))

plot_det = []
for device in devices_dictionary.values():  
    try:
        if device.md["silent"] == 'True':
            if device.name == 'accelerator':
                plot_det.append(device.current)
            else:
                plot_det.append(device)
            print(f"Device: {device.name} added to the list 'plot_detectors'.")
        else:
            pass
    except KeyError as e:
        print(f"Device: {device.name} NOT added to the list 'plot_detectors' {e}.")

if len(plot_det)>0:        
    ps = PlotSelect(RE,get_ipython().user_ns, plot_det)
    plotselect = ps.plotselect
        

get_ipython().register_magics(BlueskyMagicsBessy(RE, 
                                get_ipython(), 
                                database_name ="client", 
                                label_axes_dict=devices_dict))