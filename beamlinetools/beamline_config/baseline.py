from bessyii.default_detectors import (  
    SupplementalDataSilentDets,
    close_silent,
    init_silent,
)

from .base import *  
from .beamline import *  

print('\n\nLOADING baseline.py')

# create a list of baseline devices, i.e. devices to be read before and after run.
baseline = []
for device in devices_dictionary.values():  
    try:
        if device.md["baseline"] == 'True':
            baseline.append(device)
            print(f"Device: {device.name} added to the list 'baseline'.")
        else:
            print(f"Device: {device.name} NOT added to the list 'baseline'.")
    except KeyError as e:
        print(f"Device: {device.name} NOT added to the list 'baseline' {e}.")
        
# create a list of silent devices, i.e. devices to be read during each scan, but not plotted unless explicitly set in the plan.
silent_devices = []
for device in devices_dictionary.values():  
    try:
        if device.md["silent"] == 'True':
            if device.name == 'accelerator':
                silent_devices.append(device.current)
            else:
                silent_devices.append(device)
            print(f"Device: {device.name} added to the list 'silent_devices'.")
        else:
            print(f"Device: {device.name} NOT added to the list 'silent_devices'.")
    except KeyError as e:
        print(f"Device: {device.name} NOT added to the list 'silent_devices' {e}.")

RE.register_command("init_silent", init_silent)  
RE.register_command("close_silent", close_silent)  

# add supplemental data to preprocessor
sdd = SupplementalDataSilentDets(baseline=baseline, silent_devices=silent_devices)
RE.preprocessors.append(sdd)  
