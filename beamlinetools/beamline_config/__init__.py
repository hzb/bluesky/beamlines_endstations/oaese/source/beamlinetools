from .base import *
from .beamline import *
from .plans import *
from .tools import *
from .baseline import *
# from .authentication_and_metadata import *
from .magics import *
from .data_callbacks import *

# this block is deleting the functions, so that we can use the magics that have the same name
for name in plan_names:
    exec(f'del {name}')