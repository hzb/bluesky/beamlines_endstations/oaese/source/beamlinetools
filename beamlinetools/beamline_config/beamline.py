"""Module:      beamline.py

Description:    Instantiate beamline devices. Instatiation is based on device configuration y(a)ml file and rml file.
                Global variables are created to have direct access to instantiated devices using just instance names, e.g. v3.
                Access to instantiated devices by means of a dictionary "devices_dictionary" is always possible, e.g. devices_dictionary["v3"]
"""

# Import functions necessary for beamline device instantiation
from bessyii_devices_instantiation.beamline_devices import (  
    instantiate,
    wait_for_device_connection,
)

from bessyii.utils.helper import add_to_global_scope


print('\n\nLOADING beamline.py')


# Instantiate beamline devices
devices_dictionary: dict[object] = instantiate()

# Wait for instantiated devices to be connected
wait_for_device_connection(devices_dictionary)

# Create global variables from the "devices"
add_to_global_scope(devices_dictionary, globals())

# importing sim devices for testing
from ophyd.sim import det, noisy_det, motor

from ophyd.sim import SynAxis, Syn2DGauss, SynGauss
from ophyd import Device, Component as Cpt

temp_controller = SynAxis(name="temp_controller", events_per_move = 10,delay = 5, labels={"motors"})

class GasHandling(Device):

    mfc1 = Cpt(SynAxis, name = 'mfc1')
    mfc2 = Cpt(SynAxis, name = 'mfc2')
    mfc3 = Cpt(SynAxis, name = 'mfc3')
    pressure = Cpt(SynAxis, name = 'pressure')

gas_system = GasHandling("",name='gas_handling' )

class SampleStage(Device):

    x = Cpt(SynAxis)
    y = Cpt(SynAxis)

sample_stage = SampleStage(name='sample_stage')

test_motor1 = sample_stage.x
test_motor2 = sample_stage.y

mono = SynAxis(name = 'mono', egu='eV', events_per_move = 10, delay=1)

i1 = Syn2DGauss('i1', sample_stage.x, 'sample_stage_x', sample_stage.y, 'sample_stage_y', center=(3,4), Imax=10,noise='uniform',sigma=1, noise_multiplier=0.1 )
i2 = SynGauss('i2',mono, 'mono', center=5000, Imax = 1000, noise='uniform',sigma=1, noise_multiplier=0.9 )

bruker_mca = bruker.mca
chan_hv = chan.hv





