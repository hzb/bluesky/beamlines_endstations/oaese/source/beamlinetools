
# Define here data export callbacks
from datetime import datetime


from .base import *

from beamlinetools.callbacks.file_exporter import CSVCallback


year = str(datetime.now().year)


# data manager, export scans to single csv file

dm = CSVCallback(file_path="/opt/bluesky/data")
dm.change_user("oaese_"+year)
changeuser=dm.change_user
RE.subscribe(dm.receiver)


from event_model import RunRouter
import suitcase.specfile
from suitcase.specfile import Serializer

# Export everything to individual files
def spec_single_factory(name, start_doc):

    serializer = Serializer(spec_factory.directory,file_prefix='oaese_'+str(start_doc['scan_id']))

    return [serializer], []


# Export everything to a single file
# https://github.com/NSLS-II-CHX/profile_collection/blob/15202d1019def2a0132f0bdb74f54cb5450e8117/startup/99-bluesky.py#L258-L299 
def spec_factory(name, start_doc):

    serializer = Serializer(spec_factory.directory, file_prefix=spec_factory.file_prefix, flush=True)
    return [serializer], []


def new_spec_file(name):
    """
    set new specfile name:
    - path is fixed at /home/xf11id/specfiles/
    - name= xyz .spec will be added automatically
    calling sequence: new_spec_file(name='xyz')
    """
    spec_factory.file_prefix = name


# Initialize the filename to today's date.
import time
spec_factory.file_prefix = 'oaese_'+time.strftime('%Y')
spec_factory.directory = '/opt/bluesky/data/'


from event_model import RunRouter
rr = RunRouter([spec_factory])

RE.subscribe(rr)
