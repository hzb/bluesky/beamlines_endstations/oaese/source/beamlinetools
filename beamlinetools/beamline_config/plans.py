from bluesky.plans import count as _count, grid_scan as _grid_scan, scan as _scan
from bluesky.plan_stubs import mov as _mov, movr as _movr
from beamlinetools.plans.flying import flyscan as _flyscan

# import scans modified from us
from .beamline import *

from bluesky_queueserver import parameter_annotation_decorator
@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "motor1": {
            "annotation": "motor1",
            "enums": {"motor1": ["end_station_motors.x","test_motor1"]},
            "convert_device_names": True,
        },

        "motor2": {
            "annotation": "motor2",
            "enums": {"motor2": ["end_station_motors.z","test_motor2"]},
            "convert_device_names": True,
        },
        "start1": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "stop1": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "start2": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "stop2": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "num1": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },
        "num2": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        }
    }
})
def grid_scan(detectors,motor1,motor2,start1:float=0.0, stop1:float=0.0, num1:int=10,start2:float=0.0, stop2:float=0.0, num2:int=10,snake:bool=False, *, md:dict=None):

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.set(2)

    yield from _grid_scan(detectors, motor1, start1, stop1, num1, motor2, start2, stop2, num2,md=md)  

@parameter_annotation_decorator({
    "parameters": {
        "motor": {
            "annotation": "motor",
            "enums": {"motor": ["end_station_motors.x", "end_station_motors.z", "ue48_pgm.en", "u17_dcm.en","v13", "v12"]},
            "convert_device_names": True,
        },
        "position": {
            "default": 0.0,
            "min": -100000,
            "max": 100000,
            "step": 0.001,
        },
        
    }
})
def move(motor,position:float=0.0, md:dict=None):
    yield from _mov(motor, position)  


@parameter_annotation_decorator({
    "parameters": {
        "motor": {
            "annotation": "motor1",
            "enums": {"motor1": ["end_station_motors.x", "end_station_motors.z", "ue48_pgm.en", "u17_dcm.en","v13", "v12"]},
            "convert_device_names": True,
        },
        "distance": {
            "default": 0.0,
            "min": -100000,
            "max": 100000,
            "step": 0.001,
        },
        
    }
})
def rel_move(motor,distance:float=0.0, md:dict=None):
    yield from _movr(motor, distance)  

# 1D scan for endstation x, z or filters
@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "motor": {
            "annotation": "motor1",
            "enums": {"motor1": ["end_station_motors.x", "end_station_motors.z","end_station_motors.filters", "chan.hv"]},
            "convert_device_names": True,
        },
        "start": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "stop": {
            "default": 0.0,
            "min": -4000,
            "max": 4000,
            "step": 0.1,
        },
        "num": {
            "default": 10,
            "min": 0,
            "max": 200,
            "step": 1,
        },

    }
})
def scan(detectors,motor,start:float=0.0, stop:float=0.0, num:int=10,*, md:dict=None):

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.set(2)

    yield from _scan(detectors, motor, start, stop, num,md=md)  


@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "num": {
            "default": 10,
            "min": 0,
            "max": 1000000,
            "step": 1,
        },
    }
})
def count(detectors, num:int=10,delay:float=0.0, md:dict=None):

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.set(2)

    yield from _count(detectors, num,delay=delay,md=md)  

# Flyscan UE48 PGM XAS
@parameter_annotation_decorator({
    "parameters": {
    "i0": {
            "annotation": "i0",
            "enums": {"i0": ["kth00", "kth01","kth19","kth20"]},
            "convert_device_names": True,
        },
    
    "start_energy": {
            "default": 400,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "stop_energy": {
            "default": 450,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "energy_velocity": {
            "default": 0.1,
            "min": 0.01,
            "max": 0.3,
            "step": 0.01,
        },
    }
})
def ue48_pgm_flyxas(i0,use_bruker:bool=False,start_energy:float=400, stop_energy:float=450, energy_velocity:float=0.1, md:dict=None):
    """
    move the ue48_pgm from start energy to stop energy at a constant energy velocity

    Detectors will be acquired as fast as possible given their individual integration times

    detectors kth00, kth01, kth19, kth20 and the channeltron will always be read.
    
    The bruker is optionally read since it has a longer integration time

    Parameters
    ----------
    i0 : enum
        one of kth00,kth01,kth19, kth20
    use_bruker : bool
        if enabled, also read from the bruker
    start_energy: float
        the energy to start the monochromator at
    stop_energy: float 
        the energy to stop the monochromator at
    energy_velocity : float
        the rate to move the energy at
    kth_int_time : float
        the time in ms to integrate
    md : dict, optional
        metadata

    Notes
    ----

    """

    md = md or {}

    detectors = [kth00,kth01,kth19,kth20,chan]
    techniques = ['XAS_TFY']
    if use_bruker:
        detectors.append(bruker.mca)
        techniques.append('XAS_PFY')

    _md = {'i0': i0.name,
           'detectors' : [det.name for det in detectors],
           'mono': ue48_pgm.en.name,
           'start_energy': start_energy,
           'stop_energy': stop_energy,
           'energy_velocity': energy_velocity,
           'plan_name': 'ue48_pgm_flyxas',
           'techniques': techniques,
           'hints': {}
           }

    _md.update(md or {})

    #configure keithleys for speed
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.set(1)
        
    yield from _flyscan(detectors=detectors, flyer=ue48_pgm.en, start=start_energy, stop=stop_energy, vel=energy_velocity, md=None)

# Stepwise UE48 PGM XAS
@parameter_annotation_decorator({
    "parameters": {
    "i0": {
            "annotation": "i0",
            "enums": {"i0": ["kth00", "kth01","kth19", "kth20"]},
            "convert_device_names": True,
        },
    
    "start_energy": {
            "default": 400,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "stop_energy": {
            "default": 450,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "step": {
            "default": 0.1,
            "min": 0.001,
            "max": 10,
            "step": 0.01,
        },
    }
})
def ue48_pgm_xas(i0,use_bruker:bool=False,start_energy:float=400, stop_energy:float=450, step:float=0.01, md:dict=None):
    """
    move the ue48_pgm from start energy to stop energy with a step size "step" in eV

    detectors kth00, kth01, kth19, kth20 and the channeltron will always be read.
    
    The bruker is optionally read since it has a longer integration time

    Parameters
    ----------
    i0 : enum
        one of kth00,kth01,kth19, kth20
    use_bruker : bool
        if enabled, also read from the bruker
    start_energy: float
        the energy to start the monochromator at
    stop_energy: float 
        the energy to stop the monochromator at
    energy_velocity : float
        the rate to move the energy at
    md : dict, optional
        metadata

    Notes
    ----

    """

    md = md or {}

    detectors = [kth00,kth01,kth19,kth20,chan]
    techniques = ['XAS_TFY']
    if use_bruker:
        detectors.append(bruker.mca)
        techniques.append('XAS_PFY')
    
    import numpy as np
    num = np.ceil((np.abs(stop_energy-start_energy))/step)+1

    _md = {'i0': i0.name,
           'detectors' : [det.name for det in detectors],
           'mono': ue48_pgm.en.name,
           'start_energy': start_energy,
           'stop_energy': stop_energy,
           'energy_step': step,
           'num': num,
           'plan_name': 'ue48_pgm_xas',
           'techniques': techniques,
           'hints': {}
           }

    _md.update(md or {})

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.set(2)
        
    yield from _scan(detectors, ue48_pgm.en, start_energy, stop_energy, num, md=None)

# Stepwise U17 DCM XAS
@parameter_annotation_decorator({
    "parameters": {
    "i0": {
            "annotation": "i0",
            "enums": {"i0": ["kth00", "kth01","kth25","kth26"]},
            "convert_device_names": True,
        },
    
    "start_energy": {
            "default": 400,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "stop_energy": {
            "default": 450,
            "min": 10,
            "max": 400000,
            "step": 1,
        },
    "step": {
            "default": 0.1,
            "min": 0.001,
            "max": 10,
            "step": 0.01,
        },
    }
})
def u17_dcm_xas(i0,use_bruker:bool=False,start_energy:float=400, stop_energy:float=450, step:float=0.01, md:dict=None):
    """
    move the u17_dcm from start energy to stop energy with a step size "step" in eV

    detectors kth00, kth01, kth25 and kth26 and the channeltron will always be read.
    
    The bruker is optionally read since it has a longer integration time

    Parameters
    ----------
    i0 : enum
        one of kth00,kth01,kth25, kth26
    use_bruker : bool
        if enabled, also read from the bruker
    start_energy: float
        the energy to start the monochromator at
    stop_energy: float 
        the energy to stop the monochromator at
    energy_velocity : float
        the rate to move the energy at
    md : dict, optional
        metadata

    Notes
    ----

    """

    md = md or {}



    detectors = [kth00,kth01,kth25,kth26,chan]
    techniques = ['XAS_TFY']
    if use_bruker:
        detectors.append(bruker.mca)
        techniques.append('XAS_PFY')
    
    import numpy as np
    num = np.ceil((np.abs(stop_energy-start_energy))/step)+1

    _md = {'i0': i0.name,
           'detectors' : [det.name for det in detectors],
           'mono': u17_dcm.en.name,
           'start_energy': start_energy,
           'stop_energy': stop_energy,
           'energy_step': step,
           'num': num,
           'plan_name': 'u17_dcm_xas',
           'techniques': techniques,
           'hints': {}
           }

    _md.update(md or {})

    #configure keithleys for reliable readings
    for detector in detectors:
        if hasattr(detector, "trigger_rep"):
            detector.trigger_rep.set(2)
        
    yield from _scan(detectors, u17_dcm.en, start_energy, stop_energy, num, md=None)
