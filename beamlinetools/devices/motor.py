from ophyd import Device, EpicsMotor, EpicsSignalRO
from ophyd import Component as Cpt

# the two-theta motor

class OaeseMotors(Device):
    x            = Cpt(EpicsMotor, 'MTR:X', labels={"motors"})
    y            = Cpt(EpicsMotor, 'MTR:Y', labels={"motors"})
    z            = Cpt(EpicsMotor, 'MTR:Z', labels={"motors"})
    filters      = Cpt(EpicsMotor, 'FILT:M0', labels={"motors"})
    mesh         = Cpt(EpicsMotor, 'MESH:M0', labels={"motors"})


